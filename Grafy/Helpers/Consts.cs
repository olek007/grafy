﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy.Helpers
{
    public class Consts : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private static readonly Consts instance = new Consts();
        private Consts() { }

        public static Consts Instance
        {
            get
            {
                return instance;
            }
        }


        public static int numberOfWeights = 3;
        public static int maxWeightValue = 100;
        public static int propabilityOfConnection = 70;
        public static int numberOfVertex = 10;


        public static int numberOfIndividual { get; set; } = 100;
        public static int numberOfTournamentSelection { get; set; } = 3;
        public static int newIndividualsInPopulation { get; set; } = 0;
        public static int iteration { get; set; } = 1000;
        public static int propabilityOfMutation { get; set; } = 100;
        public static int numberOfElite { get; set; } = 1;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
