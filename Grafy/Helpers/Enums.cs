﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy.Helpers
{
    enum MutationType
    {
        InvertMutation,
        EndForEndSwapMutaton,
        ShiftMutation,
        RandomChangeMutation
    }
}
