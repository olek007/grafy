﻿using Grafy.Helpers;
using Grafy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy
{
    static class Generator
    {
        public static Graph MainGraph { get; set; }

        public static void Generate(int vertexNuber)
        {
            MainGraph = new Graph(vertexNuber);
            List<Node> nodes = new List<Node>();

            for (int i = 0; i < vertexNuber; i++)
            {
                MainGraph.Nodes.Add(new Node());
            }

            for (int i = 0; i < vertexNuber; i++)
            {
                for (int j = 0; j < vertexNuber; j++)
                {
                    if (StaticRandom.Next(0, 100) < Consts.propabilityOfConnection && i != j)
                    {
                        MainGraph.Nodes[i].CreateEdge(MainGraph.Nodes[j]);
                    }
                }
            }

            MainGraph.ConvertToMatrix();
        }
    }
}
