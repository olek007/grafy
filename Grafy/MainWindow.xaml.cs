﻿using Grafy.DrawIng;
using Grafy.Genetic;
using Grafy.Helpers;
using Grafy.WPFChart3D;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Grafy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MutationType mutationType;
        public bool firstFitness = true;
        public bool secondFitness = true;
        public bool thirdFitness = true;

        // transform class object for rotate the 3d model
        public WPFChart3D.TransformMatrix m_transformMatrix = new WPFChart3D.TransformMatrix();

        // ***************************** 3d chart ***************************
        private WPFChart3D.Chart3D m_3dChart;       // data for 3d chart
        public int m_nChartModelIndex = -1;         // model index in the Viewport3d
        public int m_nSurfaceChartGridNo = 100;     // surface chart grid no. in each axis
        public int m_nScatterPlotDataNo = 5000;     // total data number of the scatter plot

        // ***************************** selection rect ***************************
        ViewportRect m_selectRect = new ViewportRect();
        public int m_nRectModelIndex = -1;


        public MainWindow()
        {
            InitializeComponent();
            Generator.Generate(Consts.numberOfVertex);
            Consts consts = Consts.Instance;
            SetupDefaults();

            DrawGraph.Init(Generator.MainGraph.Nodes, c_graph);
            DrawGraph.Draw();

            // selection rect
            m_selectRect.SetRect(new Point(-0.5, -0.5), new Point(-0.5, -0.5));
            WPFChart3D.Model3D model3d = new WPFChart3D.Model3D();
            ArrayList meshs = m_selectRect.GetMeshes();
            m_nRectModelIndex = model3d.UpdateModel(meshs, null, m_nRectModelIndex, this.mainViewport);

            // display surface chart
            //ScatterPlot(1000);
            //TransformChart();

        }



        private void btn_Start_Click(object sender, RoutedEventArgs e)
        {
            Thread t = new Thread(new ThreadStart(DoGeneric));
            t.Start();
        }


        public void DoGeneric()
        {

            Population population = new Population(Consts.numberOfIndividual);
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                lb_populations.Items.Clear();
            }));

            for (int i = 0; i < Consts.iteration; i++)
            {
                population.Elite(firstFitness,secondFitness,thirdFitness);

                switch (mutationType)
                {
                    case MutationType.InvertMutation:
                        population.InvertMutation();
                        break;
                    case MutationType.EndForEndSwapMutaton:
                        population.EndForEndSwapMutaton();
                        break;
                    case MutationType.ShiftMutation:
                        population.ShiftMutation();
                        break;
                    case MutationType.RandomChangeMutation:
                        population.RandomChangeMutation();
                        break;
                    default:
                        break;
                }

                population.TournamentSelection(firstFitness,secondFitness,thirdFitness);
                population = new Population(population.NewIndividuals);

                //population.CalculateFitness();
                //Thread.Sleep(1);
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    population.CalculateFitness(firstFitness, secondFitness, thirdFitness);
                    StringBuilder sb = new StringBuilder();
                    ListBoxItem lbi = new ListBoxItem();
                    lbi.Selected += Lbi_Selected;
                    sb.Append("Iteration: " + i + " \tBest: " + population.TheBest().Fitness + " --- " + population.TheBest().Chromosomes[0].Fitness + ": " + population.TheBest().Chromosomes[1].Fitness + ": " + population.TheBest().Chromosomes[2].Fitness + " \tWorst: " + population.TheWorst().Fitness + " \tAverage: " + population.Average() + "\r\n");
                    lbi.Content = sb.ToString();
                    lbi.DataContext = population;
                    lb_populations.Items.Add(lbi);
                    //lb_populations.Items.Refresh();
                    sb.Clear();
                }), System.Windows.Threading.DispatcherPriority.DataBind);

            }
        }

        private void Lbi_Selected(object sender, RoutedEventArgs e)
        {

            c_graph.Children.Clear();
            DrawGraph.Draw();

            ListBoxItem lbi = sender as ListBoxItem;
            Population population = (lbi.DataContext as Population);
            Individual individual = population.TheBest();
            DrawGraph.InitIndividual(individual);
            DrawGraph.DrawPath();





            ScatterPlot(population);
            //TransformChart();

        }

        private void rb_InvertMutation_Checked(object sender, RoutedEventArgs e)
        {
            mutationType = MutationType.InvertMutation;
        }

        private void rb_EndForEndSwapMutaton_Checked(object sender, RoutedEventArgs e)
        {
            mutationType = MutationType.EndForEndSwapMutaton;
        }

        private void rb_ShiftMutation_Checked(object sender, RoutedEventArgs e)
        {
            mutationType = MutationType.ShiftMutation;
        }

        private void rb_RandomChangeMutation_Checked(object sender, RoutedEventArgs e)
        {
            mutationType = MutationType.RandomChangeMutation;
        }

        private void SetupDefaults()
        {
            tb_Individual.LostKeyboardFocus += tb_LostKeyboardFocus;
            tb_TournamentSelection.LostKeyboardFocus += tb_LostKeyboardFocus;
            tb_NewIndividuals.LostKeyboardFocus += tb_LostKeyboardFocus;
            tb_Mutation.LostKeyboardFocus += tb_LostKeyboardFocus;
            tb_Iteration.LostKeyboardFocus += tb_LostKeyboardFocus;
        }


        private void tb_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBox tb = (sender as TextBox);
            if (tb.Text.Count() == 0)
            {
                tb.Text = "0";
            }
            else
            {
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void cbFirst_Checked(object sender, RoutedEventArgs e)
        {
            firstFitness = true;
        }

        private void cbFirst_Unchecked(object sender, RoutedEventArgs e)
        {
            firstFitness = false;
        }

        private void cbSecond_Checked(object sender, RoutedEventArgs e)
        {
            secondFitness = true;
        }

        private void cbSecond_Unchecked(object sender, RoutedEventArgs e)
        {
            secondFitness = false;
        }

        private void cbThird_Checked(object sender, RoutedEventArgs e)
        {
            thirdFitness = true;
        }

        private void cbThird_Unchecked(object sender, RoutedEventArgs e)
        {
            thirdFitness = false;
        }

        private void ScatterPlot(Population population)
        {
            List<Individual> individuals = population.Individuals;

            // 1. set scatter chart data no.
            m_3dChart = new ScatterChart3D();
            m_3dChart.SetDataNo(individuals.Count);

            // 2. set property of each dot (size, position, shape, color)
            Random randomObject = new Random();
            int nDataRange = individuals.Count;
            for (int i = 0; i < individuals.Count; i++)
            {
                ScatterPlotItem plotItem = new ScatterPlotItem();

                plotItem.w = 10;
                plotItem.h = 10;

                plotItem.x = (float)individuals[i].Chromosomes[0].Fitness;
                plotItem.y = (float)individuals[i].Chromosomes[1].Fitness;
                plotItem.z = (float)individuals[i].Chromosomes[2].Fitness;

                plotItem.shape = 0;

                Byte nR = (Byte)255;
                Byte nG = (Byte)0;
                Byte nB = (Byte)0;

                plotItem.color = Color.FromRgb(nR, nG, nB);
                ((ScatterChart3D)m_3dChart).SetVertex(i, plotItem);
            }

            // 3. set the axes
            m_3dChart.GetDataRange();
            m_3dChart.SetAxes();

            // 4. get Mesh3D array from the scatter plot
            ArrayList meshs = ((ScatterChart3D)m_3dChart).GetMeshes();

            // 5. display model vertex no and triangle no
            UpdateModelSizeInfo(meshs);

            // 6. display scatter plot in Viewport3D
            WPFChart3D.Model3D model3d = new WPFChart3D.Model3D();
            m_nChartModelIndex = model3d.UpdateModel(meshs, null, m_nChartModelIndex, this.mainViewport);

            // 7. set projection matrix
            float viewRange = (float)1000;
            m_transformMatrix.CalculateProjectionMatrix(0, viewRange, 0, viewRange, 0, viewRange, 0.25);
            TransformChart();
        }

        private void UpdateModelSizeInfo(ArrayList meshs)
        {
            int nMeshNo = meshs.Count;
            int nChartVertNo = 0;
            int nChartTriangelNo = 0;
            for (int i = 0; i < nMeshNo; i++)
            {
                nChartVertNo += ((Mesh3D)meshs[i]).GetVertexNo();
                nChartTriangelNo += ((Mesh3D)meshs[i]).GetTriangleNo();
            }
            //labelVertNo.Content = String.Format("Vertex No: {0:d}", nChartVertNo);
            //labelTriNo.Content = String.Format("Triangle No: {0:d}", nChartTriangelNo);
        }

        // this function is used to rotate, drag and zoom the 3d chart
        private void TransformChart()
        {
            if (m_nChartModelIndex == -1) return;
            ModelVisual3D visual3d = (ModelVisual3D)(this.mainViewport.Children[m_nChartModelIndex]);
            if (visual3d.Content == null) return;
            Transform3DGroup group1 = visual3d.Content.Transform as Transform3DGroup;
            group1.Children.Clear();
            group1.Children.Add(new MatrixTransform3D(m_transformMatrix.m_totalMatrix));
        }

        public void OnViewportMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs args)
        {
            Point pt = args.GetPosition(mainViewport);
            if (args.ChangedButton == MouseButton.Left)         // rotate or drag 3d model
            {
                m_transformMatrix.OnLBtnDown(pt);
            }
            else if (args.ChangedButton == MouseButton.Right)   // select rect
            {
                m_selectRect.OnMouseDown(pt, mainViewport, m_nRectModelIndex);
            }
        }

        public void OnViewportMouseMove(object sender, System.Windows.Input.MouseEventArgs args)
        {
            Point pt = args.GetPosition(mainViewport);

            if (args.LeftButton == MouseButtonState.Pressed)                // rotate or drag 3d model
            {
                m_transformMatrix.OnMouseMove(pt, mainViewport);

                TransformChart();
            }
            else if (args.RightButton == MouseButtonState.Pressed)          // select rect
            {
                m_selectRect.OnMouseMove(pt, mainViewport, m_nRectModelIndex);
            }
            else
            {
                /*
                String s1;
                Point pt2 = m_transformMatrix.VertexToScreenPt(new Point3D(0.5, 0.5, 0.3), mainViewport);
                s1 = string.Format("Screen:({0:d},{1:d}), Predicated: ({2:d}, H:{3:d})", 
                    (int)pt.X, (int)pt.Y, (int)pt2.X, (int)pt2.Y);
                this.statusPane.Text = s1;
                */
            }
        }

        public void OnViewportMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs args)
        {
            Point pt = args.GetPosition(mainViewport);
            if (args.ChangedButton == MouseButton.Left)
            {
                m_transformMatrix.OnLBtnUp();
            }
            else if (args.ChangedButton == MouseButton.Right)
            {
                if (m_nChartModelIndex == -1) return;
                // 1. get the mesh structure related to the selection rect
                MeshGeometry3D meshGeometry = WPFChart3D.Model3D.GetGeometry(mainViewport, m_nChartModelIndex);
                if (meshGeometry == null) return;

                // 2. set selection in 3d chart
                m_3dChart.Select(m_selectRect, m_transformMatrix, mainViewport);

                // 3. update selection display
                m_3dChart.HighlightSelection(meshGeometry, Color.FromRgb(200, 200, 200));
            }
        }
    }
}
