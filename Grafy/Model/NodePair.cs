﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy.Model
{
    class NodePair
    {
        public Node Node_1 { get; set; }
        public Node Node_2 { get; set; }
        public NodePair(Node node_1, Node node_2)
        {
            Node_1 = node_1;
            Node_2 = node_2;
        }
    }
}
