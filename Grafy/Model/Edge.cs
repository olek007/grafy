﻿using Grafy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy.Model
{
    class Edge
    {
        public NodePair PairOfNodes { get; set; }
        public List<int> Weights { get; set; } = new List<int>();
        public void RandomWeights()
        {
            for (int i = 0; i < Consts.numberOfWeights; i++)
            {
                Weights.Add(StaticRandom.Next(1, Consts.maxWeightValue));
            }
        }

        public void AssignNodes(Node node_1, Node node_2)
        {
            if (node_1 != node_2)
            {
                PairOfNodes = new NodePair(node_1, node_2);
            }
        }
    }
}
