﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Grafy.Model
{
    class Node
    {
        public int Id { get; private set; }
        public List<Edge> Edges { get; set; }
        public Point Location { get; set; }
        private static int nextId { get; set; } = 0;

        public Node()
        {
            Id = nextId;
            nextId++;
            Edges = new List<Edge>();
            Location = new Point();
        }

        public void CreateEdge(Node nodeToConnect)
        {
            Edge edge = new Edge();
            edge.RandomWeights();
            edge.AssignNodes(this, nodeToConnect);
            Edges.Add(edge);

            nodeToConnect.Edges.Add(edge);
        }
    }
}
