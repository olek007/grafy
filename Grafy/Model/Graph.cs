﻿using Grafy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy.Model
{
    class Graph
    {
        public bool[][] connectionMatrix;
        public int[][][] weightMatrix;

        public List<Node> Nodes { get; set; }

        public Graph(int vertexNumber)
        {
            Nodes = new List<Node>();

            //init connectionMatrix
            connectionMatrix = new bool[vertexNumber][];
            for (int i = 0; i < vertexNumber; i++)
            {
                connectionMatrix[i] = new bool[vertexNumber];
            }

            //init weightMatrix
            weightMatrix = new int[Consts.numberOfWeights][][];
            for (int i = 0; i < Consts.numberOfWeights; i++)
            {
                weightMatrix[i] = new int[vertexNumber][];
                for (int j = 0; j < vertexNumber; j++)
                {
                    weightMatrix[i][j] = new int[vertexNumber];
                }
            }
        }

        public void ConvertToMatrix()
        {
            foreach (var node in Nodes)
            {
                foreach (var edge in node.Edges)
                {
                    connectionMatrix[edge.PairOfNodes.Node_1.Id][edge.PairOfNodes.Node_2.Id] = true;
                    connectionMatrix[edge.PairOfNodes.Node_2.Id][edge.PairOfNodes.Node_1.Id] = true;
                }
            }

            foreach (var node in Nodes)
            {
                foreach (var edge in node.Edges)
                {
                    for (int i = 0; i < Consts.numberOfWeights; i++)
                    {
                        weightMatrix[i][edge.PairOfNodes.Node_1.Id][edge.PairOfNodes.Node_2.Id] = edge.Weights[i];
                        weightMatrix[i][edge.PairOfNodes.Node_2.Id][edge.PairOfNodes.Node_1.Id] = edge.Weights[i];
                    }
                }
            }

            for (int i = 0; i < Consts.numberOfWeights; i++)
            {
                for (int j = 0; j < Consts.numberOfVertex; j++)
                {
                    for (int k = 0; k < Consts.numberOfVertex; k++)
                    {
                        if (weightMatrix[i][j][k] == 0)
                        {
                            weightMatrix[i][j][k] = 999;
                        }
                    }
                }
            }
        }

    }
}
