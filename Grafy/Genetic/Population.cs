﻿using Grafy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Grafy.Genetic
{
    class Population
    {
        public List<Individual> Individuals { get; set; }
        public List<Individual> NewIndividuals { get; set; }
        public Population(int numberOfIndividuals)
        {
            Individuals = new List<Individual>();
            for (int i = 0; i < numberOfIndividuals; i++)
            {
                Individuals.Add(new Individual());
            }
        }

        public Population(List<Individual> NewPopulation)
        {
            Individuals = new List<Individual>(NewPopulation);
            Individuals = new List<Individual>();

            foreach (var individual in NewPopulation)
            {
                Individuals.Add(new Individual(individual));
            }
        }

        public void AddNewIndividuals(int number)
        {
            for (int i = 0; i < number; i++)
            {
                NewIndividuals.Add(new Individual());
            }
        }

        public void CalculateFitness(bool f1, bool f2, bool f3)
        {
            foreach (var individual in Individuals)
            {
                individual.CalculateFitness(f1,f2,f3);
            }
            Individuals = Individuals.OrderBy(x => x.Fitness).ToList();
        }

        public void InvertMutation()
        {
            foreach (var individual in Individuals)
            {
                individual.InvertMutation();
            }
        }

        public void EndForEndSwapMutaton()
        {
            foreach (var individual in Individuals)
            {
                individual.EndForEndSwapMutaton();
            }
        }

        public void ShiftMutation()
        {
            foreach (var individual in Individuals)
            {
                individual.ShiftMutation();
            }
        }

        public void RandomChangeMutation()
        {
            foreach (var individual in Individuals)
            {
                individual.RandomChangeMutation();
            }
        }

        public void Elite(bool f1, bool f2, bool f3)
        {
            CalculateFitness(f1,f2,f3);
            NewIndividuals = new List<Individual>();
            List<Individual> temp = Individuals.OrderBy(x => x.Fitness).ToList();
            for (int i = 0; i < Consts.numberOfElite; i++)
            {
                NewIndividuals.Add(new Individual(temp.First()));
                temp.RemoveAt(0);
            }
        }

        public void TournamentSelection(bool f1, bool f2, bool f3)
        {
            CalculateFitness(f1,f2,f3);
            List<Individual> Selected = new List<Individual>();
            //NewIndividuals = new List<Individual>();
            List<Individual> temp = new List<Individual>(Individuals);

            //include the best one
            //NewIndividuals.Add(Individuals.OrderBy(x => x.Fitness).First());

            //-1
            for (int i = 0; i < Consts.numberOfIndividual - Consts.newIndividualsInPopulation - Consts.numberOfElite; i++)
            {
                temp = new List<Individual>(Individuals);
                for (int j = 0; j < Consts.numberOfTournamentSelection; j++)
                {
                    int random = StaticRandom.Next(Individuals.Count - Selected.Count);
                    Selected.Add(new Individual(temp[random]));
                    //temp.Remove(temp[random]);
                }

                //Selected = Selected.OrderBy(x => x.Fitness).ToList();

                NewIndividuals.Add(new Individual(Selected.OrderBy(x => x.Fitness).First()));

                Selected.Clear();
            }
            AddNewIndividuals(Consts.newIndividualsInPopulation);

            NewIndividuals = NewIndividuals.OrderBy(x => x.Fitness).ToList();
        }

        public void CrossingOX()
        {
            for (int i = 0; i < Individuals.Count; i += 2)
            {
                Individual I1 = Individuals[i];
                Individual I2 = Individuals[i + 1];
                Crossing(ref I1, ref I2);
            }
        }

        private void Crossing(ref Individual I1, ref Individual I2)
        {
            for (int i = 0; i < Consts.numberOfWeights; i++)
            {
                List<int> G1 = new List<int>(I1.Chromosomes[i].Genes);
                List<int> G2 = new List<int>(I2.Chromosomes[i].Genes);

                int[] Child_1 = new int[G1.Count];
                int[] Child_2 = new int[G2.Count];

                int cut_1 = StaticRandom.Next(0, G1.Count / 2);
                int cut_2 = StaticRandom.Next(G2.Count / 2, G2.Count);

                int start = Math.Min(cut_1, cut_2);
                int distance = Math.Abs(cut_1 - cut_2);

                for (int j = 0; j < Consts.numberOfVertex; j++)
                {
                    Child_1[j] = -1;
                    Child_2[j] = -1;
                }

                for (int j = start; j < start + distance; j++)
                {
                    Child_1[j] = G2[j];
                    Child_2[j] = G1[j];
                }

                for (int j = 0; j < G1.Count; j++)
                {
                    if (Child_1.Contains(G1[j]))
                    {
                        G1.RemoveAt(j);
                        j--;
                    }
                }

                for (int j = 0; j < G2.Count; j++)
                {
                    if (Child_2.Contains(G2[j]))
                    {
                        G2.RemoveAt(j);
                        j--;
                    }
                }

                for (int j = 0; j < Consts.numberOfVertex; j++)
                {
                    if (Child_1[j] == -1)
                    {
                        Child_1[j] = G1.First();
                        G1.RemoveAt(0);
                    }
                }

                for (int j = 0; j < Consts.numberOfVertex; j++)
                {
                    if (Child_2[j] == -1)
                    {
                        Child_2[j] = G2.First();
                        G2.RemoveAt(0);
                    }
                }

                I1.Chromosomes[i].Genes = new List<int>(Child_1.ToList());
                I2.Chromosomes[i].Genes = new List<int>(Child_2.ToList());
            }
        }

        public Individual TheBest()
        {
            return Individuals.OrderBy(x => x.Fitness).First();
        }
        public Individual TheWorst()
        {
            return Individuals.OrderBy(x => x.Fitness).Last();
        }
        public float Average()
        {
            return Individuals.Sum(x => x.Fitness) / (float)Individuals.Count;
        }

    }
}
