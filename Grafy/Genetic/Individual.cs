﻿using Grafy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy.Genetic
{
    class Individual
    {
        public List<Chromosome> Chromosomes { get; set; }
        public List<int> Genes { get; set; }

        public int Fitness { get; set; }
        public Individual()
        {
            Chromosomes = new List<Chromosome>();
            for (int i = 0; i < Consts.numberOfWeights; i++)
            {
                int[][] weightMatrix = Generator.MainGraph.weightMatrix[i];
                Chromosomes.Add(new Chromosome(weightMatrix));
            }

            Genes = new List<int>();
            List<int> temp = Finder.findRandomCycle(Chromosomes.First().weightMatrix);
            foreach (var item in temp)
            {
                Genes.Add(item);
            }
            foreach (var chromosome in Chromosomes)
            {
                chromosome.Genes = new List<int>();
                for (int i = 0; i < Genes.Count; i++)
                {
                    chromosome.Genes.Add(Genes[i]);
                }
            }
        }
        public Individual(Individual individual)
        {
            Chromosomes = new List<Chromosome>();
            foreach (var chromosome in individual.Chromosomes)
            {
                Chromosomes.Add(new Chromosome(chromosome));
            }
            Genes = new List<int>();
            for (int i = 0; i < individual.Genes.Count; i++)
            {
                Genes.Add(individual.Genes[i]);
            }
            Fitness = individual.Fitness;
        }

        public void CalculateFitness(bool f1, bool f2, bool f3)
        {
            Fitness = 0;
            if (f1)
            {
                Fitness += Chromosomes[0].CalculateFitness();
            }
            if (f2)
            {
                Fitness += Chromosomes[1].CalculateFitness();

            }
            if (f3)
            {
                Fitness += Chromosomes[2].CalculateFitness();

            }
            //foreach (var chromosome in Chromosomes)
            //{
            //    Fitness += chromosome.CalculateFitness();
            //}
        }

        public void InvertMutation()
        {
            int cut_1 = StaticRandom.Next(Genes.Count);
            int cut_2 = StaticRandom.Next(Genes.Count);

            int start = Math.Min(cut_1, cut_2);
            int distance = Math.Abs(cut_1 - cut_2);

            Genes.Reverse(start, distance);

            foreach (var chromosome in Chromosomes)
            {
                chromosome.Genes = Genes;
            }
        }

        public void EndForEndSwapMutaton()
        {
            int cut = StaticRandom.Next(Genes.Count);

            List<int> temp = Genes.GetRange(0, cut);
            Genes.RemoveRange(0, cut);
            Genes.InsertRange(Genes.Count, temp);
            foreach (var chromosome in Chromosomes)
            {
                chromosome.Genes = Genes;
            }
        }

        public void ShiftMutation()
        {
            int select = StaticRandom.Next(Genes.Count);
            int insert = StaticRandom.Next(Genes.Count);

            int temp = Genes[select];
            Genes.RemoveAt(select);
            Genes.Insert(insert, temp);
            foreach (var chromosome in Chromosomes)
            {
                chromosome.Genes = Genes;
            }
        }

        public void RandomChangeMutation()
        {
            int numberOfMutations = StaticRandom.Next(Genes.Count / 2);
            List<int> temp = new List<int>();

            for (int i = 0; i < numberOfMutations; i++)
            {
                int randomTakePoint = StaticRandom.Next(Genes.Count);
                temp.Add(Genes[randomTakePoint]);
                Genes.RemoveAt(randomTakePoint);
            }

            for (int i = 0; i < numberOfMutations; i++)
            {
                int randomInsertPoint = StaticRandom.Next(Genes.Count);
                Genes.Insert(randomInsertPoint, temp.First());
                temp.RemoveAt(0);
            }

            foreach (var chromosome in Chromosomes)
            {
                chromosome.Genes = Genes;
            }
        }


    }
}
