﻿using Grafy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy.Genetic
{
    class Finder
    {
        public static List<int> path = new List<int>();
        public static List<int> findRandomCycle(int[][] weightMatrix)
        {
            path.Clear();
            while (path.Count < weightMatrix.Length)
            {
                int number = 0;
                int nextNumber;
                List<int> availableConnecton = new List<int>();
                path = new List<int>();
                path.Add(number);

                for (int i = 1; i < weightMatrix.Length; i++)
                {
                    availableConnecton.Add(i);
                }

                for (int i = 1; i < weightMatrix.Length; i++)
                {
                    nextNumber = availableConnecton[StaticRandom.Next(0, availableConnecton.Count)];
                    if (weightMatrix[number][nextNumber] > 0)
                    {
                        number = nextNumber;
                        path.Add(number);
                        availableConnecton.Remove(nextNumber);
                    }
                }

                if (weightMatrix[path.First()][path.Last()] <= 0)
                {
                    path.Clear();
                }
            }
            return path;
        }

        public static string displayPath()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < path.Count; i++)
            {
                sb.Append(" " + path[i] + " ");
            }
            return sb.ToString();
        }
    }
}
