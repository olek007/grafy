﻿using Grafy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy.Genetic
{
    class Chromosome
    {
        public int[][] weightMatrix;
        public List<int> Genes { get; set; }
        public int Fitness { get; set; }

        public Chromosome(int[][] weights)
        {
            weightMatrix = weights;
            List<int> temp = Finder.findRandomCycle(weightMatrix);
        }
        public Chromosome(Chromosome chromosome)
        {
            weightMatrix = chromosome.weightMatrix;

            for (int i = 0; i < weightMatrix.Length; i++)
            {
                for (int j = 0; j < weightMatrix.Length; j++)
                {
                    weightMatrix[i][j] = chromosome.weightMatrix[i][j];
                }
            }

            Genes = new List<int>();
            for (int i = 0; i < chromosome.Genes.Count; i++)
            {
                Genes.Add(chromosome.Genes[i]);
            }
        }

        public int CalculateFitness()
        {
            int node;
            int nextNode;
            int weight;
            Fitness = 0;

            for (int i = 0; i < weightMatrix.Length; i++)
            {
                node = Genes[i];
                nextNode = Genes[(i + 1) % weightMatrix.Length];
                weight = weightMatrix[node][nextNode];
                Fitness += weight;
            }
            return Fitness;
        }
    }
}
