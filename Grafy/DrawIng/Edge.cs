﻿using Grafy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Grafy.DrawIng
{
    class Edge : DrawingObject
    {
        public Edge(Canvas canvas) : base(canvas)
        {
            
        }



        public void DrawLine(Node N1, Node N2)
        {
            DrawLine(N1.Location, N2.Location);
        }

        public void DrawLine(Point P1, Point P2)
        {
            DrawLine(P1.X, P1.Y, P2.X, P2.Y);
        }

        public void DrawLine(double x1, double y1, double x2, double y2)
        {
            MyShape = new Line();
            SolidColorBrush brush = new SolidColorBrush(DrawingColor);
            MyShape.Stroke = brush;
            Thickness = 2;
            MyShape.StrokeThickness = Thickness;
            (MyShape as Line).X1 = x1;
            (MyShape as Line).Y1 = y1;
            (MyShape as Line).X2 = x2;
            (MyShape as Line).Y2 = y2;
            Can.Children.Add(MyShape);
        }
    }
}
