﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Grafy.DrawIng
{
    abstract class DrawingObject
    {
        public int Thickness { get; set; }
        public Color DrawingColor { get; set; }
        protected Canvas Can { get; set; }
        protected Shape MyShape { get; set; }
        protected SolidColorBrush MySolidColorBrush { get; set; }

        public DrawingObject(Canvas canvas)
        {
            Can = canvas;
            MySolidColorBrush = new SolidColorBrush();
            DrawingColor = Colors.Black;
        }
    }
}
