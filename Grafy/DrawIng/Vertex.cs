﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;

namespace Grafy.DrawIng
{
    class Vertex : DrawingObject
    {
        public Vertex(Canvas canvas) : base(canvas)
        {
        }
        public void Draw(double x, double y)
        {
            MyShape = new Ellipse();
            MySolidColorBrush.Color = DrawingColor;
            MyShape.Fill = MySolidColorBrush;
            Thickness = 25;
            MyShape.Width = Thickness;
            MyShape.Height = Thickness;
            Canvas.SetLeft(MyShape, x - Thickness / 2);
            Canvas.SetTop(MyShape, y - Thickness / 2);
            Can.Children.Add(MyShape);
        }
    }
}
