﻿using Grafy.Genetic;
using Grafy.Helpers;
using Grafy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Grafy.DrawIng
{
    static class DrawGraph
    {
        public static List<Node> Nodes { get; set; }
        public static Canvas Can { get; set; }
        public static Individual individual { get; set; }

        public static void Init(List<Node> nodes, Canvas canvas)
        {
            Nodes = nodes;
            Can = canvas;
        }

        public static void InitIndividual(Individual indiv)
        {
            individual = indiv;
        }

        public static void Draw()
        {
            double radius = Math.Min(Can.Width, Can.Height) / 3;
            double diffAngle = 360.0d / Nodes.Count;
            double centerX = Can.Width / 2;
            double centerY = Can.Height / 2;

            Vertex DrawVertex = new Vertex(Can);

            for (int i = 0; i < Nodes.Count; i++)
            {
                double degree = (Math.PI * diffAngle / 180) * i;
                double x = centerX + (radius * Math.Cos(degree));
                double y = centerY + (radius * Math.Sin(degree));
                Nodes[i].Location = new Point(x, y);
                DrawVertex.DrawingColor = Colors.Red;
                DrawVertex.Draw(x, y);
            }

            Edge AllDrawEdge = new Edge(Can);

            for (int i = 0; i < Nodes.Count; i++)
            {
                for (int j = i; j < Nodes.Count; j++)
                {
                    Point P1 = Nodes[i].Location;
                    Point P2 = Nodes[j].Location;
                    AllDrawEdge.DrawingColor = Colors.Red;
                    AllDrawEdge.DrawLine(P1, P2);
                }
            }



            Edge DrawEdge = new Edge(Can);

            foreach (var node in Nodes)
            {
                foreach (var edge in node.Edges)
                {
                    Point P1 = edge.PairOfNodes.Node_1.Location;
                    Point P2 = edge.PairOfNodes.Node_2.Location;
                    DrawEdge.DrawingColor = Colors.Black;
                    DrawEdge.DrawLine(P1, P2);
                }
            }

        }

        public static void DrawPath()
        {
            Edge DrawPath = new Edge(Can);
            List<int> genes = individual.Genes;

            for (int i = 0; i < genes.Count; i++)
            {
                Point P1 = Nodes.Find(x => x.Id == genes[i % genes.Count]).Location;
                Point P2 = Nodes.Find(x => x.Id == genes[(i + 1) % genes.Count]).Location;
                DrawPath.DrawingColor = Colors.Violet;
                DrawPath.DrawLine(P1, P2);
            }
        }

    }
}
